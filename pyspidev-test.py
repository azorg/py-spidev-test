#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import spidev
spi = spidev.SpiDev()
spi.open(0, 0) # /dev/spidev0.0
spi.max_speed_hz = 100000
spi.mode = 0x01 # CPOL | CPHA

send = [0x01, 0x02, 0x03, 0x04]
recv = spi.xfer(send)
print([("0x%X" % i) for i in recv])

spi.close()


