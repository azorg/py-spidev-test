SPI python test
===============

1. Go to https://github.com/doceme/py-spidev
```
git clone https://github.com/doceme/py-spidev
cd py-spidev
sudo python3 setup.py install
```

2. Install CH341A SPI linux kernel module
   from https://github.com/gschorcht/spi-ch341-usb
```
sudo apt-get install dkms
git clone https://github.com/gschorcht/spi-ch341-usb.git
cd spi-ch341-usb
make
sudo make install
```

3. sudo chmod a+rw /dev/spidevX.Y



